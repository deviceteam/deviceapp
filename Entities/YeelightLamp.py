from yeelight import Bulb


class YeeLamp:
    bulb = None
    ip = ""
    status = False
    r = None
    g = None
    b = None
    brightness = None
    id = None

    def __init__(self, ip, status=None, r=None, g=None, b=None, brightness=None, id=None):
        self.ip = ip
        self.status = status
        self.r = r
        self.g = g
        self.b = b
        self.brightness = brightness
        self.id = id
        self.bulb = Bulb(ip)

    def turn_on(self):
        self.bulb.turn_on()

    def turn_off(self):
        self.bulb.turn_off()

    def set_brightness(self):
        self.bulb.set_brightness(self.brightness)

    def set_rgb(self):
        self.bulb.set_rgb(self.r, self.g, self.b)
