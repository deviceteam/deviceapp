
class DeviceRepository:

    device_arr = []

    def add_device(self, device):
        self.device_arr.append(device)
        return True

    def delete_lamp(self, device):
        if len(self.device_arr) != 0:
            self.device_arr.remove(device)

    def find_lamp(self, lamp):
        if len(self.device_arr) != 0:
            return self.device_arr.index(lamp)

    def get_lamp_list(self):
        return self.device_arr

    def change_status(self, lamp):
        if len(self.device_arr) != 0:
            return lamp.toggle()
