import json
import time
import requests
from Repository.DeviceRepository import DeviceRepository
from collections import namedtuple
from YeelightOption.YeeOpt import YeelightOption

ip_address = "http://192.168.1.114:8080/device"
devRep = DeviceRepository

def THMOption(opt, thm):
    pass

while True:
    try:
        print("Sending...")
        check_queue = requests.get(ip_address)
    except requests.exceptions.ConnectionError:
        print("Connection refused")
        exit(-1)

    if check_queue.status_code is not 204:
        yeeop = YeelightOption()
        data_object = json.loads(check_queue.text, object_hook=lambda d: namedtuple('lamp', d.keys())(*d.values()))
        if data_object.body.ip not in devRep.device_arr:
            if data_object.body.type == "Yeelight": 
                yeeop.option(lamp=data_object, ip=data_object.body.ip)
            elif data_object.body.type == "THM":
                pass
        else:
            if data_object.body.type == "Yeelight":
                index = devRep.device_arr.index(data_object.body.ip)
                yeeop.option(data_object, devRep.device_arr[index])
            elif data_object.body.type == "THM":
                pass
    else:
        print("Waiting for request...")
        time.sleep(2)
