from Entities.YeelightLamp import YeeLamp
import requests

class YeelightOption:
    post = "http://10.4.54.129:9000/post"

    def HTMLColorToRGB(self, colorstring):
        """ convert #RRGGBB to an (R, G, B) tuple """
        colorstring = colorstring.strip()
        if colorstring[0] == '#': colorstring = colorstring[1:]
        if len(colorstring) != 6:
            raise ValueError("input #%s is not in #RRGGBB format" % colorstring)
        r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
        r, g, b = [int(n, 16) for n in (r, g, b)]
        return (r, g, b)

    def option(self, lamp, ip=None):
        if ip is not None:

            if lamp.body.lamp_body.state is True:
                yee = YeeLamp(ip=lamp.body.ip, status=lamp.body.lamp_body.state, id=lamp.guid)
                print("ON")
                yee.turn_on()
                answer_to_queue = {"guid": lamp.guid, "status": "ok"}
                requests.post(url=self.post, json=answer_to_queue)
                # if lamp.body.lamp_body.color is not '-1':
                r, g, b = self.HTMLColorToRGB(lamp.body.lamp_body.color)
                print(str(r)+ str(g)+ str(b))
                yee = YeeLamp(ip=lamp.body.ip, status=lamp.body.lamp_body.state, r=r, g=g, b=b, id=lamp.guid)
                yee.set_rgb()
                print("RGB")

                if lamp.body.lamp_body.brightness is not -1:
                    yee = YeeLamp(ip=lamp.body.ip, status=lamp.body.lamp_body.state, brightness=lamp.body.lamp_body.brightness, id=lamp.guid)
                    yee.set_brightness()
            else:
                yee = YeeLamp(ip=lamp.body.ip, status=lamp.body.lamp_body.state, id=lamp.guid)
                yee.turn_off()
                print("OFF")
                answer_to_queue = {"guid": lamp.guid, "status": "ok"}
                requests.post(url=self.post, json=answer_to_queue)


